(function(){
    'use strict';

    angular
	.module('app',[
	    'main',
	    'auth',
	    'calculator'
	]);
})();
