var gulp = require('gulp');
var webserver = require('gulp-webserver');

gulp.task('webserver',function(){
    gulp.src('')
	.pipe(webserver({
	    host: '0.0.0.0',
	    port: '8080',
	    livereload: false,
	    open: true
	}));
});


gulp.task('default', function(){
    gulp.run('webserver');
});
